package doppel

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"time"
)

type Config struct {
	SMTP struct {
		Host string `json:"host"`
		Port string `json:"port"`
		User string `json:"user"`
		Pass string `json:"pass"`
	}
	MailTo    []string      `json:"mail_to"`
	TmplPath  string        `json:"template_path"`
	EmailMins time.Duration `json:"email_minutes"`
	Nick      string        `json:"nick"`
	NickMatch string        `json:"nick_match"`
	Password  string        `json:"password"`
	Channels  []string      `json:"channels"`
	Server    struct {
		Address string `json:"address"`
		Port    string `json:"port"`
		SSL     bool   `json:"ssl"`
	} `json:"server"`
	Responses      map[string]map[string]string `json:"responses"`
	TimeoutMessage string                       `json:"timeout_message"`
	TimeoutSeconds time.Duration                `json:"timeout_seconds"`
}

func readConfig(path string) *Config {
	config := new(Config)

	log.Printf("Reading Config at: %s", path)

	file, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal("Error:", err)
	}

	err = json.Unmarshal(file, &config)
	if err != nil {
		log.Fatal(err)
	}

	return config
}
