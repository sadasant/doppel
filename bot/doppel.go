package doppel

import (
	"html/template"
	"log"
	"regexp"
	"time"
)

const Version = "v0.0.15"

type Doppel struct {
	IRC    *IRC
	Config *Config

	EmailTemplate *template.Template
	NickRegexp    *regexp.Regexp

	Nick         string
	Mentions     []*IRC_Line
	ChannelsLeft map[string]bool
	Timeouts     map[string]int64
	Watching     []*Watch

	quit chan bool
}

func New(config_path string) *Doppel {
	config := readConfig(config_path)

	D := &Doppel{
		IRC:           NewIRC(),
		Config:        config,
		EmailTemplate: template.Must(template.ParseFiles(config.TmplPath)),
		NickRegexp:    regexp.MustCompile(config.NickMatch),
		Nick:          config.Nick,
		Mentions:      []*IRC_Line{},
		ChannelsLeft:  map[string]bool{},
		Timeouts:      map[string]int64{},
		Watching:      []*Watch{},
	}

	return D
}

func (D *Doppel) Run() {
	println("DOPPEL", Version)
	log.Printf("Config: %v", D.Config)

	D.quit = make(chan bool)

	// watchNames is in watcher.go, as it uses the watching list
	go D.Watch(15 * time.Second)

	// sendEmail is in emails.go
	go D.sendEmail()

	// Connecting the IRC
	D.IRC.Connect(D)

	<-D.quit
}

func (D *Doppel) Quit() {
	D.IRC.Sendf("QUIT")
	D.quit <- true
}

func (D *Doppel) getResponse(nick, msg string) (string, [2]string, bool) {
	var matches [2]string
	for match_nick, responses := range D.Config.Responses {
		if nick == match_nick || match_nick == "*" {
			matches[0] = match_nick
			for match, resp := range responses {
				regx := regexp.MustCompile(match)
				if regx.MatchString(msg) {
					matches[1] = match
					return resp, matches, true
				}
			}
		}
	}
	return "", matches, false
}
