package doppel

import (
	"bytes"
	"log"
	"net/smtp"
	"strings"
	"time"
)

type EmailBody struct {
	From     string
	To       string
	Version  string
	Config   *Config
	Mentions []*IRC_Line
}

// Send saved lines to the Config.MailTo
func (D *Doppel) sendEmail() {
	for {
		select {
		case <- D.quit:
			return
		default:
			time.Sleep(D.Config.EmailMins*time.Minute)

			if len(D.Mentions) == 0 {
				continue
			}

			SMTP := D.Config.SMTP

			buffer := new(bytes.Buffer)
			D.EmailTemplate.Execute(buffer, EmailBody{
				SMTP.User,
				strings.Join(D.Config.MailTo, ", "),
				Version,
				D.Config,
				D.Mentions,
			})

			auth := smtp.PlainAuth("", SMTP.User, SMTP.Pass, SMTP.Host)
			err := smtp.SendMail(SMTP.Host+":"+SMTP.Port, auth, SMTP.User, D.Config.MailTo, buffer.Bytes())
			if err != nil {
				log.Printf("SMTP ERROR: %v", err)
				continue
			}
			log.Printf("EMAIL SENT | %v messages", len(D.Mentions))

			// Clearing saved lines
			D.Mentions = []*IRC_Line{}
		}
	}
}
