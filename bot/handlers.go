package doppel

import (
	"strings"
	"time"
)

// handlers are all the handlers we'll use, according to IRC replies or errors.
// To read a full list of IRC numeric codes, go to: <https://www.alien.net.au/irc/irc2numerics.html>
func (irc *IRC) Handle(D *Doppel, l *IRC_Line) {
	config := D.Config

	switch l.Code {

	// NOTICE is sent many times, but we'll only care about two,
	// one which tells us we've successfully authentified,
	// and the other who says that our nick is not registered.
	// For any of both, if we have the Config.Nick
	// join every channel in the list of channels
	case "NOTICE":
		identified := "You are now identified for"
		not_registered := "is not a registered nickname."
		if strings.Index(l.Message, identified) != -1 || strings.Index(l.Message, not_registered) != -1 {
			for _, channel := range config.Channels {
				irc.Sendf("JOIN  %s", channel)
			}
		}
		break

	// NICK is sent when the user changes nick.
	// If we have changed nick,
	// save the value locally
	case "NICK":
		if l.Nick == D.Nick {
			D.Nick = l.Chan[1:]
		}
		break

	// There are two ways of doing PINGs, one is this, the other is with a PRIVMSG
	// We'll PING back
	case "PING":
		irc.Sendf("PONG %s", l.Nick)
		break

	// PRIVMSG are any kind of message directly sent to the client.
	// We'll care about CTCP PINGs and user messages.
	// There are also others, like VERSION, but freenode doesn't support it.
	case "PRIVMSG":
		if strings.SplitN(l.Message, " ", 2)[0] == "PING" {
			irc.Sendf("NOTICE %s :\001PING %s\001", l.Nick, l.Message)
			return
		}
		// Append the message to the list of mentions
		// if it was a private message.
		if l.Chan == D.Nick {
			D.Mentions = append(D.Mentions, l)
			return
		}
		// Continue only if our nick have been mentioned
		nick_starts := strings.Index(l.Message, D.Nick)
		if nick_starts == -1 {
			return
		}
		// Appending the line to the list of mentions
		D.Mentions = append(D.Mentions, l)
		// Removing our nick
		// plus the ':' if it's after the nick
		nick_ends := nick_starts + len(D.Nick) - 1
		if l.Message[nick_ends] == ':' {
			nick_ends++
		}
		msg := l.Message[:nick_starts] + l.Message[nick_ends:]
		// Check if we have predefined responses
		// Return if the match wasn't generic
		if res, matches, ok := D.getResponse(l.Nick, msg); ok {
			irc.Sendf("PRIVMSG %s :%s: %s", l.Chan, l.Nick, res)
			if matches[0] != "*" {
				return
			}
		}

		// Send a timeout message (only if it's defined in the config)
		if config.TimeoutMessage != "" {
			timeout_name := l.Chan + ":" + l.Nick
			timeout := time.Now().Unix()
			D.Timeouts[timeout_name] = timeout
			time.Sleep(config.TimeoutSeconds * time.Second)
			// Return if some other goroutine changed the last_timeout
			if D.Timeouts[timeout_name] != timeout {
				return
			}
			irc.Sendf("PRIVMSG %s :%s: %s", l.Chan, l.Nick, config.TimeoutMessage)
			delete(D.Timeouts, timeout_name)
		}
		break

	// RPL_WHOISUSER is sent after we send WHOIS
	// If the config.Nick answered,
	// then clear the watching list
	case "311":
		nick := strings.SplitN(l.Message, " ", 2)[0]
		if nick == config.Nick {
			D.Watching = []*Watch{}
			D.ChannelsLeft = map[string]bool{}
		}
		break

	// RPL_NAMREPLY is sent after we send NAMES
	case "353":
		parts := strings.Split(l.Message, " ")
		channel := strings.ToLower(parts[1])
		names := parts[2:]
		names[0] = names[0][1:] // Removing the ":"

		for i := 0; i < len(names); i++ {
			nick := names[i]
			// If one of the connected users have a name that matches with config.NickMatch
			// Add it to the watching list and to the list of D.ChannelsLeft
			if nick != D.Nick && D.NickRegexp.MatchString(nick) {
				if nick != config.Nick {
					D.AddWatch(nick, channel)
				}
				irc.Sendf("PART  %s", channel)
				D.ChannelsLeft[channel] = true
				if nick != config.Nick {
					// Change nick to have a _ at the end
					nick += "_"
					irc.Sendf("NICK  %s", nick)
				}
				return
			}
		}
		break

	// ERR_NOSUCHNICK is sent if WHOIS is done to a user who's not in the server
	case "401":
		nick := strings.SplitN(l.Message, " ", 2)[0]
		joined := false
		// If the missing nick is the nick I'm supposed to have, lets recover it
		if nick == config.Nick {
			for _, channel := range config.Channels {
				channel = strings.ToLower(channel)
				if !D.ChannelsLeft[channel] {
					irc.Sendf("JOIN  %s", channel)
					joined = true
				}
			}
			if joined {
				irc.Sendf("NICK %s", config.Nick)
			}
			return
		}
		// Else, check if the nick is on the list of nicks to look up.
		// If so, join the channel and delete previously created values.
		if channel, ok := D.IsWatching(nick); ok {
			irc.Sendf("JOIN  %s", channel)
			D.RemoveWatch(nick, channel)
			delete(D.ChannelsLeft, channel)
		}
		break

	// ERR_NICKNAMEINUSE
	// If the nick is already in use, add a "_"
	case "433":
		D.Nick += "_"
		irc.Sendf("NICK %s", D.Nick)
		break

	// ERR_UNAVAILRESOURCE
	// ERR_BANNICKCHANGE
	// We'll care of this because it's sent after the server says our user is temporarily unavailable.
	// We'll release the nick and take it back
	case "437":
		irc.Sendf("PRIVMSG NickServ :release %s %s", config.Nick, config.Password)
		time.Sleep(5 * time.Second)
		irc.Sendf("NICK %s", config.Nick)
		time.Sleep(5 * time.Second)
		irc.Sendf("PRIVMSG NickServ :identify %s %s", config.Nick, config.Password)
		break

	}
}
