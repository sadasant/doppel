package doppel

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"log"
	"net"
	"strings"
)

// Inspiring source codes:
// - <https://github.com/fluffle/goirc>
// - <https://github.com/thoj/go-ircevent>

type IRC struct {
	Conn   net.Conn
	Inbox  chan string
	Outbox chan string
	quit   chan bool
}

type IRC_Line struct {
	Nick    string
	Code    string
	Chan    string
	Message string
	Raw     string
}

func NewIRC() *IRC {
	irc := &IRC{
		Inbox:  make(chan string),
		Outbox: make(chan string),
	}
	return irc
}

func (irc *IRC) Connect(D *Doppel) {
	irc.quit = D.quit

	server := D.Config.Server.Address + ":" + D.Config.Server.Port
	log.Printf("Connecting with: %s", server)
	var err error
	if D.Config.Server.SSL {
		irc.Conn, err = tls.Dial("tcp", server, new(tls.Config))
	} else {
		irc.Conn, err = net.Dial("tcp", server)
	}
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Connected with: %s", server)

	go irc.readLoop(D)
	go irc.writeLoop()

	if D.Config.Password != "" {
		irc.Sendf("PASS %s", D.Config.Password)
	}
	irc.Sendf("NICK %s", D.Config.Nick)
	irc.Sendf("USER %s 0.0.0.0 0.0.0.0 :%s", D.Config.Nick, D.Config.Nick)
}

func (irc *IRC) readLoop(D *Doppel) {
	reader := bufio.NewReaderSize(irc.Conn, 512)
FOR:
	for {
		select {
		case <-irc.quit:
			return
		default:
			s, err := reader.ReadString('\n')
			if err != nil {
				log.Print(err)
				break FOR
			}
			log.Println(s[:len(s)-2]) // Without \r\n
			l := irc.parseLine(s)
			go irc.Handle(D, l)
		}
	}
	irc.quit <- true
}

func (irc *IRC) writeLoop() {
FOR:
	for {
		select {
		case <-irc.quit:
			return
		default:
			b, ok := <-irc.Outbox
			if !ok || b == "" || irc.Conn == nil {
				break
			}

			log.Printf("SENDING: %q\n", b)

			_, err := irc.Conn.Write([]byte(b))
			if err != nil {
				log.Print(err)
				break FOR
			}
		}
	}
	irc.quit <- true
}

func (irc *IRC) parseLine(s string) *IRC_Line {
	l := new(IRC_Line)
	l.Raw = s

	s = s[:len(s)-2] //Remove \r\n

	if s[0] != ':' {
		parts := strings.SplitN(s, " ", 2)
		l.Code = parts[0]
		l.Message = parts[1]
		return l
	}

	// Removing the first ":"
	s = s[1:]

	parts := strings.SplitN(s, " ", 4)
	l.Nick = parts[0]

	if strings.Index(l.Nick, "!") != -1 {
		l.Nick = strings.SplitN(l.Nick, "!", 2)[0]
	}

	l.Code = parts[1]
	l.Chan = parts[2]

	if len(parts) > 3 {
		l.Message = parts[3]
		if l.Message[0] == ':' || l.Message[0] == '\001' {
			l.Message = l.Message[1:]
		}
		if l.Message[len(l.Message)-1] == '\001' {
			l.Message = l.Message[0 : len(l.Message)-1]
		}
	}

	return l
}

func (irc *IRC) Sendf(format string, a ...interface{}) {
	irc.Outbox <- fmt.Sprintf(format+"\r\n", a...)
}
