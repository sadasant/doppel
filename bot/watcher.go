package doppel

import (
	"log"
	"time"
)

// watching is the names to Whois until they log off

type Watch struct {
	Nick    string
	Channel string
}

func (D *Doppel) AddWatch(nick, channel string) {
	D.Watching = append(D.Watching, &Watch{nick, channel})
}

func (D *Doppel) RemoveWatch(nick, channel string) {
	for i := 0; i < len(D.Watching); i++ {
		if D.Watching[i].Nick == nick && D.Watching[i].Channel == channel {
			// D.Watching = append(D.Watching[:i], D.Watching[i+1:]...)
			copy(D.Watching[i:], D.Watching[i+1:])
			D.Watching = D.Watching[:len(D.Watching)-1]
		}
	}
}

func (D *Doppel) IsWatching(nick string) (string, bool) {
	for i := 0; i < len(D.Watching); i++ {
		if D.Watching[i].Nick == nick {
			return D.Watching[i].Channel, true
		}
	}
	return "", false
}

// Check if Config.Nick is online or not, check watched users.
func (D *Doppel) Watch(t time.Duration) {
	c := D.Config
	for {
		time.Sleep(t)

		// If I don't have my designed name, check if it's owner is still online
		// If he goes offline, we'll connect back.
		// Check the ERR_NOSUCHNICK handler (handlers.go).
		if D.Nick != c.Nick {
			D.IRC.Sendf("WHOIS  %s", c.Nick)
		}

		// Checking the status of the nicks to look up.
		// If one of them logs off, we'll get back to the channel.
		// Check the ERR_NOSUCHNICK handler (handlers.go).
		log.Printf("watching: %v", D.Watching)
		for _, w := range D.Watching {
			D.IRC.Sendf("WHOIS  %s", w.Nick)
		}

		// Check who's in each Config's channel,
		// only if we haven't left it.
		for _, channel := range c.Channels {
			if left, ok := D.ChannelsLeft[channel]; ok && left {
				continue
			}
			D.IRC.Sendf("NAMES  %s", channel)
		}
	}
}
