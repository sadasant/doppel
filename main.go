package main

import (
	"bitbucket.org/sadasant/doppel/bot"
	"os"
)

func main() {
	D := doppel.New(os.Args[1])
	D.Run()
	D.Quit()
}
